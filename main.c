#include <avr/io.h>
#include <avr/interrupt.h>
#define SEG_A		PB0
#define SEG_B		PB1
#define SEG_C		PB2
#define SEG_D		PB3
#define SEG_E		PB4
#define SEG_F		PB5
#define SEG_G		PB6

#define BEEP		PD7
#define HEAT		PD5
#define DIG_1		PD4
#define DIG_2		PD3
#define DIG_3		PD2
#define HL_TEMP		PD1
#define HL_SET		PD0

#define HL_TIMER	PC1
#define SB1_TX		PC2
#define SB2_TX		PC3
#define SB3_RX		PC4
#define SB4_RX		PC5
#define ADC_TEMP	PC0


#define clockCyclesPerMicrosecond() ( F_CPU / 1000000L )
#define clockCyclesToMicroseconds(a) ( ((a) * 1000L) / (F_CPU / 1000L) )
#define MICROSECONDS_PER_TIMER0_OVERFLOW (clockCyclesToMicroseconds(64 * 256))
#define MILLIS_INC (MICROSECONDS_PER_TIMER0_OVERFLOW / 1000)
#define FRACT_INC ((MICROSECONDS_PER_TIMER0_OVERFLOW % 1000) >> 3)
#define FRACT_MAX (1000 >> 3)

volatile unsigned long timer0_overflow_count = 0;
volatile unsigned long timer0_millis = 0;
static unsigned char timer0_fract = 0;

ISR (TIMER0_OVF_vect)
{
	unsigned long m = timer0_millis;
	unsigned char f = timer0_fract;
	
	m += MILLIS_INC;
	f += FRACT_INC;
	if (f >= FRACT_MAX) {
		f -= FRACT_MAX;
		m += 1;
	}

	timer0_fract = f;
	timer0_millis = m;
	timer0_overflow_count++;
}

unsigned long millis()
{
	unsigned long m;
	uint8_t oldSREG = SREG;	
	cli();
	m = timer0_millis;
	SREG = oldSREG;

	return m;
}

unsigned long micros() {
	unsigned long m;
	uint8_t oldSREG = SREG, t;
	
	cli();
	m = timer0_overflow_count;

	t = TCNT0;
	if ((TIFR0 & _BV(TOV0)) && (t < 255))
		m++;

	SREG = oldSREG;
	
	return ((m << 8) + t) * (64 / clockCyclesPerMicrosecond());
}

int main(void)
{
	DDRB = (1<<SEG_A)|(1<<SEG_B)|(1<<SEG_C)|(1<<SEG_D)|(1<<SEG_E)|(1<<SEG_F)|(1<<SEG_G);
	PORTB = 0x00;
	
	DDRC = (1<<HL_TIMER)|(1<<SB1_TX)|(1<<SB2_TX);
	PORTC = 0x00;
	
	DDRD = (1<<HL_TEMP)|(1<<HL_SET)|(1<<DIG_1)|(1<<DIG_2)|(1<<DIG_3)|(1<<HEAT)|(1<<BEEP);
	PORTD = 0x00;
	
	TCCR0A = 0x00;
	TCCR0B = (1<<CS01)|(1<<CS00);
	TIMSK0 = (1<<TOIE0);

#if defined(ADCSRA)
	// set a2d prescaler so we are inside the desired 50-200 KHz range.
	#if F_CPU >= 16000000 // 16 MHz / 128 = 125 KHz
		ADCSRA = (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	#elif F_CPU >= 8000000 // 8 MHz / 64 = 125 KHz
		ADCSRA = (1<<ADPS2)|(1<<ADPS1)|(0<<ADPS0);
	#elif F_CPU >= 4000000 // 4 MHz / 32 = 125 KHz
		ADCSRA = (1<<ADPS2)|(0<<ADPS1)|(1<<ADPS0);
	#elif F_CPU >= 2000000 // 2 MHz / 16 = 125 KHz
		ADCSRA = (1<<ADPS2)|(0<<ADPS1)|(0<<ADPS0);
	#elif F_CPU >= 1000000 // 1 MHz / 8 = 125 KHz
		ADCSRA = (0<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	#else // 128 kHz / 2 = 64 KHz -> This is the closest you can get, the prescaler is 2
		ADCSRA = (0<<ADPS2)|(0<<ADPS1)|(1<<ADPS0);
	#endif
	// enable a2d conversions
	ADCSRA |= (1<<ADEN);
#endif
	
	
    while (1) 
    {
		
    }
}

